package site.thesampleapp.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class JsonSerializer {
    private final ObjectMapper objectMapper = buildObjectMapper();
    private ObjectMapper buildObjectMapper(){
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper;
    }

    public String serialize(Object object){
        try {
            return objectMapper.writeValueAsString(object);
        }catch (Exception ex){
            throw new IllegalStateException(ex);
        }
    }

    public <T> T deSerialize(String input, Class<T> klass){
        try {
            return objectMapper.readValue(input,klass);
        }catch (Exception ex){
            throw new IllegalStateException(ex);
        }
    }
}
