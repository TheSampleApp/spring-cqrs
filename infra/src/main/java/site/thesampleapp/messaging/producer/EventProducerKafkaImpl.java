package site.thesampleapp.messaging.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import site.thesampleapp.domain.event.Event;
import site.thesampleapp.domain.event.UserCreated;
import site.thesampleapp.domain.event.UserDeleted;
import site.thesampleapp.domain.event.UserUpdated;
import site.thesampleapp.domain.messaging.EventProducer;
import site.thesampleapp.util.JsonSerializer;

@Component
@RequiredArgsConstructor
public class EventProducerKafkaImpl implements EventProducer {
    @Value("${event.topic_name}")
    private final String topicName;
    private final KafkaTemplate<String,String> kafkaTemplate;
    private final JsonSerializer jsonSerializer;
    @Override
    public void produce(Event event) {
        switch (event){
            case UserCreated userCreated ->
                    kafkaTemplate.send(topicName,userCreated.id(),jsonSerializer.serialize(userCreated));
            case UserUpdated userUpdated ->
                    kafkaTemplate.send(topicName,userUpdated.id(),jsonSerializer.serialize(userUpdated));
            case UserDeleted userDeleted ->
                    kafkaTemplate.send(topicName,userDeleted.id(),jsonSerializer.serialize(userDeleted));
            default -> throw new IllegalStateException("Unexpected value: " + event);
        }
    }
}
