package site.thesampleapp.messaging.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import site.thesampleapp.domain.command.Command;
import site.thesampleapp.domain.command.CreateUser;
import site.thesampleapp.domain.command.DeleteUser;
import site.thesampleapp.domain.command.UpdateUser;
import site.thesampleapp.domain.messaging.CommandProducer;
import site.thesampleapp.util.JsonSerializer;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class CommandProducerKafkaImpl implements CommandProducer {
    @Value("${command.topic_name}")
    private final String topicName;
    private final KafkaTemplate<String,String> kafkaTemplate;
    private final JsonSerializer jsonSerializer;
    @Override
    public void produce(Command command) {
        switch (command){
            case CreateUser createUser -> {
                String userId = UUID.randomUUID().toString();
                kafkaTemplate.send(topicName,userId,jsonSerializer.serialize(createUser));
            }
            case UpdateUser updateUser ->
                    kafkaTemplate.send(topicName,updateUser.id(),jsonSerializer.serialize(updateUser));
            case DeleteUser deleteUser ->
                    kafkaTemplate.send(topicName,deleteUser.id(),jsonSerializer.serialize(deleteUser));
            default -> throw new IllegalStateException("Unexpected value: " + command);
        }
    }
}
