package site.thesampleapp.messaging.consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import site.thesampleapp.domain.command.Command;
import site.thesampleapp.domain.command.CreateUser;
import site.thesampleapp.domain.command.DeleteUser;
import site.thesampleapp.domain.command.UpdateUser;
import site.thesampleapp.domain.event.UserCreated;
import site.thesampleapp.domain.event.UserDeleted;
import site.thesampleapp.domain.event.UserUpdated;
import site.thesampleapp.domain.service.UserEventService;
import site.thesampleapp.util.JsonSerializer;

import java.util.Date;

@Component
@Slf4j
@RequiredArgsConstructor
public class CommandConsumer {
    private final JsonSerializer jsonSerializer;
    private final UserEventService userEventService;
    @KafkaListener(topics = "${command.topic_name}",
            groupId = "${command.consumer_group}",
            containerFactory = "commandKafkaListenerContainerFactory")
    public void onCommand(ConsumerRecord<String, String> record){
        Date currentDate = new Date();
        String key = record.key();
        String commandMessage = record.value();
        Command command = jsonSerializer.deSerialize(commandMessage, Command.class);
        switch (command){
            case CreateUser createUser->
                    userEventService.onEvent(new UserCreated(key, createUser.fullName(), createUser.email(), currentDate));
            case UpdateUser updateUser ->
                    userEventService.onEvent(new UserUpdated(updateUser.id(), updateUser.fullName(),currentDate));
            case DeleteUser deleteUser ->
                    userEventService.onEvent(new UserDeleted(deleteUser.id(), currentDate));
            default -> log.error("Invalid Command : ",command);
        }
    }
}
