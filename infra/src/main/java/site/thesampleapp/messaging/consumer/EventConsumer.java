package site.thesampleapp.messaging.consumer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import site.thesampleapp.domain.entity.User;
import site.thesampleapp.domain.event.Event;
import site.thesampleapp.domain.event.UserCreated;
import site.thesampleapp.domain.event.UserDeleted;
import site.thesampleapp.domain.event.UserUpdated;
import site.thesampleapp.domain.service.UserService;
import site.thesampleapp.util.JsonSerializer;

@Component
@Slf4j
@RequiredArgsConstructor
public class EventConsumer {
    private final JsonSerializer jsonSerializer;
    private final UserService userService;
    @KafkaListener(topics = "${event.topic_name}",
            groupId = "${event.consumer_group}",
            containerFactory = "eventKafkaListenerContainerFactory")
    public void onCommand(ConsumerRecord<String, String> record){
        String eventMessage = record.value();
        Event event = jsonSerializer.deSerialize(eventMessage,Event.class);
        switch (event){
            case UserCreated userCreated ->
                    userService.save(User.builder().
                            id(userCreated.id()).
                            email(userCreated.email()).
                            fullName(userCreated.fullName()).build());
            case UserUpdated userUpdated -> userService.findById(userUpdated.id()).map(user->{
                user.setFullName(userUpdated.fullName());
                userService.save(user);
                return user;
            });
            case UserDeleted userDeleted -> userService.deleteById(userDeleted.id());
            default -> log.error("Invalid Event:",event);
        }
    }
}
