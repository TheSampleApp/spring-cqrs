package site.thesampleapp.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import site.thesampleapp.domain.command.CreateUser;
import site.thesampleapp.domain.command.DeleteUser;
import site.thesampleapp.domain.command.UpdateUser;
import site.thesampleapp.domain.entity.User;
import site.thesampleapp.domain.messaging.CommandProducer;
import site.thesampleapp.domain.service.UserService;

@RequiredArgsConstructor
public class UserRestController {
    private final UserService userService;
    private final CommandProducer commandProducer;
    @GetMapping("/")
    public Flux<User> findAll() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    public Mono<User> findUserById(@PathVariable("id") String id) {
        return userService.findById(id);
    }

    @PostMapping("/")
    public void saverUser(@RequestParam(name = "fullName")String fullName,
                                @RequestParam(name = "email") String email) {
        CreateUser createUser = new CreateUser(fullName,email);
        commandProducer.produce(createUser);
    }

    @PutMapping("/{id}")
    public void updateUser(@RequestParam(name = "id")String id,
                                @RequestParam(name = "fullName")String fullName) {
        UpdateUser updateUser = new UpdateUser(id,fullName);
        commandProducer.produce(updateUser);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") String id) {
        commandProducer.produce(new DeleteUser(id));
    }

}
