package site.thesampleapp.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import site.thesampleapp.domain.entity.EventData;
import site.thesampleapp.domain.event.Event;
import site.thesampleapp.domain.messaging.EventProducer;
import site.thesampleapp.domain.repository.EventDataRepository;
import site.thesampleapp.domain.service.UserEventService;
import site.thesampleapp.util.JsonSerializer;

@Component
@RequiredArgsConstructor
public class UserEventServiceImpl implements UserEventService {
    private final EventDataRepository eventDataRepository;
    private final EventProducer eventProducer;
    private final JsonSerializer jsonSerializer;
    @Override
    public void onEvent(Event event) {
        String data = jsonSerializer.serialize(event);
        EventData eventData = EventData.builder().
                id(event.getId()).
                time(event.getTime()).
                data(data).
                build();
        eventDataRepository.save(eventData);
        eventProducer.produce(event);
    }
}
