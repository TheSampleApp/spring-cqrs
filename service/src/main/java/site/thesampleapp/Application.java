package site.thesampleapp;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import site.thesampleapp.config.KafkaConfiguration;

@SpringBootApplication
@Import(KafkaConfiguration.class)
public class Application {
}
