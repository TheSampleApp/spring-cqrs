package site.thesampleapp.domain.entity;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table
public class User {
    @PrimaryKey
    private String id;
    private String fullName;
    private String email;
}