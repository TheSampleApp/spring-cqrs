package site.thesampleapp.domain.repository;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import site.thesampleapp.domain.entity.User;
@Repository
public interface UserRepository extends ReactiveCassandraRepository<User, String> {
}
