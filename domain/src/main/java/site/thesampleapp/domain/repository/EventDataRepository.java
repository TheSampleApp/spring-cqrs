package site.thesampleapp.domain.repository;

import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import site.thesampleapp.domain.entity.EventData;
@Repository
public interface EventDataRepository extends ReactiveCassandraRepository<EventData, String> {
}
