package site.thesampleapp.domain.messaging;

import site.thesampleapp.domain.event.Event;

public interface EventProducer {
    void produce(Event event);
}
