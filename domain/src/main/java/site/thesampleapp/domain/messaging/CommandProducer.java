package site.thesampleapp.domain.messaging;

import site.thesampleapp.domain.command.Command;

public interface CommandProducer {
    void produce(Command command);
}
