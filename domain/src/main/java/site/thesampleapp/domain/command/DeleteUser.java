package site.thesampleapp.domain.command;

public record DeleteUser(String id) implements Command{
}
