package site.thesampleapp.domain.command;

public record UpdateUser(String id,String fullName) implements Command{
}
