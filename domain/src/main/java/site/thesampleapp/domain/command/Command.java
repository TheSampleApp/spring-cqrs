package site.thesampleapp.domain.command;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = CreateUser.class, name = "create"),
        @JsonSubTypes.Type(value = UpdateUser.class, name = "update"),
        @JsonSubTypes.Type(value = DeleteUser.class, name = "delete")
})
public interface Command {
}
