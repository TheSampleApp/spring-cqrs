package site.thesampleapp.domain.command;

public record CreateUser(String fullName, String email) implements Command{
}
