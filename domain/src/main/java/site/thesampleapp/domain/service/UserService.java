package site.thesampleapp.domain.service;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import site.thesampleapp.domain.entity.User;

public interface UserService {
    Flux<User> findAll();
    Mono<User> findById(String id);
    void save(User user);
    void deleteById(String id);
}
