package site.thesampleapp.domain.service;
import site.thesampleapp.domain.event.Event;
public interface UserEventService {
    void onEvent(Event event);
}
