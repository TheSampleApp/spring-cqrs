package site.thesampleapp.domain.event;

import java.util.Date;

public record UserDeleted(String id, Date deletionTime) implements Event {
    @Override
    public String getId() {
        return id;
    }

    @Override
    public Date getTime() {
        return deletionTime;
    }
}
