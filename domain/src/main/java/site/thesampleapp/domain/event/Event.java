package site.thesampleapp.domain.event;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = UserCreated.class, name = "created"),
        @JsonSubTypes.Type(value = UserUpdated.class, name = "updated"),
        @JsonSubTypes.Type(value = UserDeleted.class, name = "deleted")
})
public interface Event {
    String getId();
    Date getTime();
}
