package site.thesampleapp.domain.event;
import java.util.Date;
public record UserCreated(String id, String fullName, String email, Date creationTime) implements Event{
    @Override
    public String getId() {
        return id;
    }
    @Override
    public Date getTime() {
        return creationTime;
    }
}
