package site.thesampleapp.domain.event;

import java.util.Date;

public record UserUpdated (String id, String fullName, Date updateTime) implements Event{
    @Override
    public String getId() {
        return id;
    }

    @Override
    public Date getTime() {
        return updateTime;
    }
}
